//
//  ContentView.swift
//  Hackathon SwiftUI
//
//  Created by Aayush R Kumar on 3/25/23.
//

import SwiftUI

struct ContentView: View {
    @State var waterBool: Bool = false
    @State var graphBool: Bool = false
    var body: some View {
        NavigationView {
            ZStack {
               // Color(Color.white).ignoresSafeArea()
                VStack {
                    
                    Image("Image").resizable().aspectRatio(contentMode: .fit)
                    
                    
                    

                   
                    NavigationLink(destination: WaterConsumption(), isActive: $waterBool) {
                        Button("Lets Get Started") {
                            waterBool = true
                        }.buttonStyle(.borderedProminent).font(Font.custom("Louis George Cafe", size: 25)).shadow(color: .gray, radius: 4, x: 0, y: 4).tint(Color(hex: "ff49b5"))
                    }
                    Spacer().frame(height: 30)
               
                   
                    NavigationLink(destination: GraphView(), isActive: $graphBool) {
                        Button("Graph") {
                            graphBool = true
                        }.buttonStyle(.borderedProminent).font(Font.custom("Louis George Cafe", size: 25)).shadow(color: .gray, radius: 4, x: 0, y: 4).tint(Color(hex: "8649ff"))
                    }
                }
                .padding()
                Spacer()
            }
        }.navigationBarBackButtonHidden()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

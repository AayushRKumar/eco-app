//
//  WaterConsumption.swift
//  Hackathon SwiftUI
//
//  Created by Aayush R Kumar on 3/25/23.
//

import SwiftUI

struct WaterConsumption: View {
    let waterConsumptionQuestions = ["How many showers do you take per week?", "On average how long is each shower?", "Do you leave the faucet on while brushing your teeth? Type 1 for 'yes' and 0 for 'no'.", "How many Dishwasher loads do you do per week?", "How many laundry loads do you do per week?"]
    @State var answers: [String] = [String]()
    @State var answer: String = ""
    @State var questionCounter = 1
    @State var buttonText = "Submit"
    @State var tipText = ""
    @State var navActive = false
    var body: some View {
        ZStack {
           
            VStack {
                NavigationLink(destination: ContentView(), isActive: $navActive) { EmptyView() }
//                Text("Water Consumption").font(Font.custom("LEMONMILK-Regular", size: 25)).padding()
                Divider()
                
               
                HStack {
                    Text("Question \(questionCounter)").padding().font(Font.custom("LEMONMILK-Regular", size: 25))
                    Spacer()
                }
                Group {
                    Divider()
                    
                    Text(waterConsumptionQuestions[questionCounter-1]).font(Font.custom("Louis George Cafe", size: 25)).padding()
                }
                TextField("Enter Answer", text: $answer).background(RoundedRectangle(cornerRadius: 5).fill( (Color(hex: "#16dbc7")))).border(Color.black).padding().keyboardType(.numberPad).font(Font.custom("Louis George Cafe", size: 25)).padding()
                
               
                    Button(buttonText) {
                        
                        if buttonText == "Next Question" {
                            tipText = ""
                            questionCounter += 1
                            buttonText = "Submit"
                        } else if buttonText == "Submit" {
                            var answerInt = Int(answer) ?? 0
                            tipText = "That may cause you to use more water than necessary. Heres a suggestion: "
                            switch questionCounter {
                            case 1:
                                if answerInt > 7 {
                                    tipText += "Turn off the shower when washing hair"

                                } else {
                                    tipText = "You are taking a good amount of showers to conserve water!"

                                }
                                break
                            case 2:
                                if answerInt > 9 {
                                    tipText += "Take shorter showers"
                                } else {
                                    tipText = "Good job, you are taking the right amount of time for showers!"

                                }
                                break
                            case 3:
                                if answerInt == 1 {
                                    tipText += "Turn the faucet off"
                                } else {
                                    tipText = "You are saving water by keeping the faucet off!"
                                }
                                break
                            case 4:
                                if answerInt > 5 {
                                    tipText += "Use full loads and hand wash dishes as well"
                                } else {
                                    tipText = "You are saving water by doing less loads!"
                                }
                            case 5:
                                if answerInt > 5 {
                                    tipText += " Wear clothes multiple times before doing laundry and use full loads"
                                } else {
                                    tipText = "You are saving water by doing less laundry!"
                                }
                            case 6:
                                if answerInt > 2 {
                                    tipText += "Wash the car less and use less water to wash."
                                } else {
                                    tipText = "Good Job!"
                                }
                                 
                            default: break
                                
                            }
                            if !(waterConsumptionQuestions.count == questionCounter) {
                                answers.append(answer)
                                answer = ""
                                
                               buttonText = "Next Question"
                                
                            } else {
                                //Finished it
                                answers.append(answer)
                                let newData = WaterConsumptionAnswers(date: Date.now, howManyShowersWeekly: Int(answers[0])! , howLongShowers: Int(answers[1])!, faucetOn: Int(answers[2])!, howManyDishWasherLoadsWeekly: Int(answers[3])!, howManyLaundryLoadsWeekly: Int(answers[4])!)
                                if let totalData = DataManager().readData(typeOfData: TypeOfData.waterConsumption) as? [WaterConsumptionAnswers] {
                                    var dataToWrite = totalData
                                    dataToWrite.append(newData)
                                    DataManager().writeData(typeOfData: TypeOfData.waterConsumption, waterData: dataToWrite)
                                } else {
                                    DataManager().writeData(typeOfData: TypeOfData.waterConsumption, waterData: [newData])
                                }
                                
                               
                               
                                buttonText = "Finish"
                                
                            }
                        } else if buttonText == "Finish" {
                            navActive = true
                        }
                    }.buttonStyle(.borderedProminent).font(Font.custom("Louis George Cafe", size: 25)).padding().shadow(color: .gray, radius: 2, x: 0, y: 2).tint(Color(hex: "1773ce"))
                
                Divider()
                Text(tipText).font(Font.custom("Louis George Cafe", size: 25)).padding()
                Spacer()
            }
        }
        
    }
}

struct WaterConsumption_Previews: PreviewProvider {
    static var previews: some View {
        WaterConsumption()
    }
}

//
//  GraphView.swift
//  Hackathon SwiftUI
//
//  Created by Aayush R Kumar on 3/25/23.
//

import SwiftUI
import Charts
struct GraphView: View {
  
    @State var pickedValue = 0
    
    
    var body: some View  {
        VStack {
            Text("What do you want to Graph?").font(Font.custom("LEMONMILK-Regular", size: 15)).padding()
            Picker("What do you want to graph?", selection: $pickedValue) {
                Text("How many showers do you take per week?").tag(0)
                Text("On average how long is each shower?").tag(1)
                Text("How many Dishwasher loads do you do per week?").tag(2)
                Text("How many laundry loads do you do per week?").tag(3)
                Text("Total Gallons Weekly").tag(4)
            }
            Chart {
                ForEach(DataManager().readData(typeOfData: TypeOfData.waterConsumption) as? [WaterConsumptionAnswers] ?? [], id: \.self) { x in
                    if pickedValue == 0 {
                   
                        BarMark(x: .value("Date", x.date), y: .value("Y Value", x.howManyShowersWeekly))
                    } else if pickedValue == 1 {
                        BarMark(x: .value("Date", x.date), y: .value("Y Value", x.howLongShowers))
                    } else if pickedValue == 2 {
                        BarMark(x: .value("Date", x.date), y: .value("Y Value", x.howManyDishWasherLoadsWeekly))
                    } else if pickedValue == 3{
                        BarMark(x: .value("Date", x.date), y: .value("Y Value", x.howManyLaundryLoadsWeekly))
                    } else if pickedValue == 4 {
                        
                        BarMark(x: .value("Date", x.date), y: .value("Y Value", calculate(x: x)))
                    }
                     
                    
                }
            }.padding()
        }
    }
    func calculate(x: WaterConsumptionAnswers) -> Double {
        var runningTotal = 0.0
        runningTotal += Double(x.howLongShowers) * Double(2.5) * Double(x.howManyShowersWeekly)
        runningTotal += 6 * Double(x.howManyDishWasherLoadsWeekly)
        runningTotal += 17 * Double(x.howManyLaundryLoadsWeekly)
        return runningTotal
    }
}

struct GraphView_Previews: PreviewProvider {
    static var previews: some View {
        GraphView()
    }
}

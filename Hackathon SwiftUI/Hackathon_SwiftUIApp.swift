//
//  Hackathon_SwiftUIApp.swift
//  Hackathon SwiftUI
//
//  Created by Aayush R Kumar on 3/25/23.
//

import SwiftUI

@main
struct Hackathon_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

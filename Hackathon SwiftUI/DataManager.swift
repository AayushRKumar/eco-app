//
//  DataManager.swift
//  Hackathon SwiftUI
//
//  Created by Aayush R Kumar on 3/25/23.
//

import Foundation


enum TypeOfData: String {
    case waterConsumption
    case electricityConsumption
   
}
struct WaterConsumptionAnswers: Codable, Hashable {
    
    
    var date: Date
    var howManyShowersWeekly: Int
    var howLongShowers: Int
    var faucetOn: Int
    var howManyDishWasherLoadsWeekly: Int
    var howManyLaundryLoadsWeekly:Int
}
//Prebuilt from another project

class DataManager {
    
    
    public func writeData(typeOfData: TypeOfData, waterData: [WaterConsumptionAnswers]? = nil) {
        var fileName = "data"
        if (typeOfData == TypeOfData.waterConsumption) {
            fileName = "waterData"
        }
        
        let documentDirectoryUrl = try! FileManager.default.url(
            for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true
        )
        let fileUrl = documentDirectoryUrl.appendingPathComponent(fileName).appendingPathExtension("txt")
        // prints the file path
        print("File path \(fileUrl.path)")
        //data to write in file.
        
        
        
        do {
            var data: Data?
            
            if (typeOfData == TypeOfData.waterConsumption) {
                data = try JSONEncoder().encode(waterData)
            }
            
            
            var stringData = String(data: data!, encoding: .utf8)!
            
            //stringData = ""
            //To erase data
            try stringData.write(to: fileUrl, atomically: true, encoding: String.Encoding.utf8)
            
        } catch let error as NSError {
            print (error)
        }
        
    }
    
    public func readData(typeOfData: TypeOfData) -> Any {
        
        var fileName = "data"
        if (typeOfData == TypeOfData.waterConsumption) {
            fileName = "waterData"
        }
        
        let documentDirectoryUrl = try! FileManager.default.url(
            for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true
        )
        let fileUrl = documentDirectoryUrl.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        var readFile = ""
        do {
            readFile = try String(contentsOf: fileUrl)
            guard let data: Data = readFile.data(using: .utf8) else {
                print("error ")
                return "Error"
            }
            if (typeOfData == TypeOfData.waterConsumption) {
                let decode = try JSONDecoder().decode([WaterConsumptionAnswers].self, from: data)
                return decode
            } else if (typeOfData == TypeOfData.electricityConsumption) {
                return ""
            }
            
           
            
            
            
            
            
            
        } catch let error as NSError {
            print(error)
        }
        print (readFile)
        return ""
        
    }
}
